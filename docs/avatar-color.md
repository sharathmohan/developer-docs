# Avatar Color

Use the following Snippet for avatar color

```javascript
    var states = [
        'success',
        'brand',
        'danger',
        'success',
        'warning',
        'dark',
        'primary',
        'info'
    ];
    var avatarColor = function(name) {
        if(name){
            let firstChar = name.charCodeAt(0);
            let lastChar = name.charCodeAt(name.length - 3);
            if (lastChar) {
                    return (firstChar + lastChar) % 8
            } else {
                    return (firstChar) % 8
            }
        } else{
            return 0
        }
    }

```
```angular2
<div  class="kt-user-card-v2">\t\t\t\t\t\t\t\t
    <div class="kt-user-card-v2__pic">\t\t\t\t\t\t\t\t
        <div class="kt-badge kt-badge--xl kt-badge--${states[avatarColor(t.name)]}">
            ${t.name.substring(0, 1)}
        </div>\t\t\t\t\t\t\t\t
    </div>\t\t\t\t\t\t\t\t
    <div class="kt-user-card-v2__details" >\t\t\t\t\t\t\t\t
        <a style="cursor: pointer;"  href="javascript:void(0);" class="kt-user-card-v2__name"  id="processempid" value=${t.emp_id} data-processempid=${t.id}>
                ${t.name}
        </a>\t\t\t\t\t\t\t\t
        <br><span class="kt-user-card-v2__desc">${job_title}</span>\t\t\t\t\t\t\t\t
     </div>
</div>
```