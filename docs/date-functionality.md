# Date Functionality
 
 > Ensure the appropriate locale is set to avoid the wrong dates being set in accross various operating system platforms


### To Get the current date
 ```javascript
let today = new Date().toLocaleDateString('en-IN');
 ```

### To Convert date to moment

 ```javascript
 let todayMoment = moment(today, 'DD/MM/YYYY');
 ```

### To Find Date Difference

```javascript
let diff = todayMoment.diff(getDonMoment);
```
Calculates the differenece between todayMoenet  and getDonMoment

*Simply put* 
diff = todayMoment - getDonMoment


*For More Information Refer*
https://momentjs.com/

